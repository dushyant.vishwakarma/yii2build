<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="site_content">
    <h1><?= Html::encode($this->title) ?></h1>

    <p class="form-style-2-heading">Please fill out the following fields to signup:</p>

    <div class="row form-style-2">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class' => 'input-field']) ?>

                <?= $form->field($model, 'email')->textInput(['class' => 'input-field']) ?>

                <?= $form->field($model, 'password')->passwordInput(['class' => 'input-field']) ?>

                <label><span> </span><input type="submit" value="Sign Up" /></label>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
