<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="site_content">
    <h1><?= Html::encode($this->title) ?></h1>

    <p class="form-style-2-heading">Please fill out the following fields to login:</p>

    <div class="row form-style-2">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class' => 'input-field']) ?>

                <?= $form->field($model, 'password')->passwordInput(['class' => 'input-field']) ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                    <br>
                    Need new verification email? <?= Html::a('Resend', ['site/resend-verification-email']) ?>
                </div>

                <label><span> </span><input type="submit" value="Login" /></label>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
