<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="site_content">
    <h1><?= Html::encode($this->title) ?></h1>

    <p class="form-style-2-heading">
        If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
    </p>

    <div class="row form-style-2">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true,'class' => 'input-field']) ?>

                <?= $form->field($model, 'email')->textInput(['class' => 'input-field'])  ?>

                <?= $form->field($model, 'subject')->textInput(['class' => 'input-field'])  ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6,'class' => 'textarea-field']) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(['class' => 'input-field']) ) ?>

                
                <label><span> </span><input type="submit" value="Submit" /></label>

               

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
